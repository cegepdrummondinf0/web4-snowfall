using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Snowfall.Application.Services;
using Snowfall.Data.Context;
using Snowfall.Data.Repositories;
using Snowfall.Domain.Models;

namespace Snowfall.Shared;

public static class ConfigInjectionDependances
{
    public static IServiceCollection EnregistrerServices(this IServiceCollection services)
    {
        if (services == null)
            throw new ArgumentNullException(nameof(services));

        // Data
        services.AddSingleton<DapperContext>();
        services.AddScoped<IEvenementsRepository, EvenementsRepository>();
        services.AddScoped<IVilleRepository, VilleRepository>();
        services.AddScoped<IMessageRepository, MessageRepository>();
        services.AddScoped<IInscriptionRepository, InscriptionRepository>();
        services.AddScoped<IRoleStore<ApplicationRole>, RoleRepository>();
        services.AddScoped<IUserStore<ApplicationUser>, UserRepository>();
        
        // Application
        services.AddScoped<IEvenementService, EvenementService>();
        services.AddScoped<IVilleService, VilleService>();
        services.AddScoped<IMessageService, MessageService>();
        services.AddScoped<IInscriptionService, InscriptionService>();
        services.AddScoped<IPrixService, PrixService>();
        
        //Identity
        services.AddScoped<IRoleStore<ApplicationRole>, RoleRepository>();
        services.AddScoped<IUserStore<ApplicationUser>, UserRepository>();

        return services;
    }
}