using System.Data;
using Dapper;
using Snowfall.Data.Context;
using Snowfall.Domain.Models;

namespace Snowfall.Data.Repositories;

public class VilleRepository : IVilleRepository
{
    private readonly DapperContext _dbContext;

    public VilleRepository(DapperContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Ville>> GetAll()
    {
        string sql = @"
            SELECT * from villes;
        ";

        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            IEnumerable<Ville> villes = await connection.QueryAsync<Ville>(sql);
            return villes.ToList();
        }
    }
}