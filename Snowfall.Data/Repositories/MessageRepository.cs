using System.Data;
using Dapper;
using Snowfall.Data.Context;
using Snowfall.Domain.Models;

namespace Snowfall.Data.Repositories;

public class MessageRepository : IMessageRepository
{
    private readonly DapperContext _dbContext;

    public MessageRepository(DapperContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<List<Message>> FindByEvenementId(int evenementId)
    {
        string sql = @"
        SELECT * from messages c 
        INNER JOIN application_users au on au.id = c.utilisateur_id
        WHERE c.evenement_id = @EvenementId
        ORDER BY date_creation DESC
    ";

        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            IEnumerable<Message> messages = await connection.QueryAsync<Message, ApplicationUser, Message>(sql,
                (message, utilisateur) =>
                {
                    message.Utilisateur = utilisateur;
                    return message;
                },
                new
                {
                    evenementId = evenementId
                });
            return messages.ToList();
        }
    }
    
    public async Task<Message> Create(Message message)
    {
        string sql = @"
            INSERT INTO messages (evenement_id, utilisateur_id, contenu, date_creation, date_modification)
            VALUES(@EvenementId, @UtilisateurId, @Contenu, @DateCreation, @DateModification)
            RETURNING id
        ";

        message.DateCreation = DateTime.Now;
        message.DateModification = DateTime.Now;

        using (IDbConnection connection = _dbContext.CreateConnection())
        { 
            message.Id = await connection.QuerySingleAsync<int>(sql, message);
            return message;
        }
    }
    
    public async Task<Message?> FindById(int id)
    {
        string sql = @"
            SELECT * from messages
            WHERE id = @Id
        ";

        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            Message? message = await connection.QuerySingleOrDefaultAsync<Message>(sql, 
                new
                {
                    Id = id
                });
            return message;
        }
    }
    
    public async Task<bool> Update(Message message)
    {
        string sql = @"
        UPDATE messages SET
            utilisateur_id = @UtilisateurId,
            evenement_id = @EvenementId,
            contenu = @Contenu,
            date_modification = @DateModification
        WHERE id = @Id";
    
        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            message.DateModification = DateTime.Now;

            var affectedRows = await connection.ExecuteAsync(sql, message);
            return affectedRows == 1;
        }
    }
    
    public async Task<bool> Delete(int id)
    {
        string sql = @"
            DELETE FROM messages
            WHERE id = @Id
        ";
    
        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            var affectedRows = await connection.ExecuteAsync(sql, new { Id = id });
            return affectedRows == 1;
        }
    }
}