using System.Data;
using Dapper;
using Snowfall.Data.Context;
using Snowfall.Domain.Models;

namespace Snowfall.Data.Repositories;

public class EvenementsRepository : IEvenementsRepository
{
    private readonly DapperContext _dbContext; 

    public EvenementsRepository(DapperContext dbContext) 
    {
        _dbContext = dbContext; 
    }
    
    public async Task<List<Evenement>> GetAll()
    {
        string sql = @"
            SELECT * from evenements e
             INNER JOIN villes v on e.ville_id = v.id;
        ";
        
        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            IEnumerable<Evenement> evenements = await connection.QueryAsync<Evenement, Ville, Evenement>(sql,
                (evenement, ville) =>
                {                             
                    evenement.Ville = ville;  
                    return evenement;         
                });    
            return evenements.ToList();
        }
    }

    public async Task<List<Evenement>> FindByVilleId(int villeId)
    {
        string sql = @"
            SELECT * FROM evenements e
            INNER JOIN villes v on e.ville_id = v.id
            WHERE ville_id = @VilleId
        ";

        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            IEnumerable<Evenement> evenements = await connection.QueryAsync<Evenement, Ville, Evenement>(sql, 
                (evenement, ville) =>
                {                             
                    evenement.Ville = ville;  
                    return evenement;         
                },
                new
                {
                    VilleId = villeId
                });
            
            return evenements.ToList();
        }
    }

    public async Task<List<Evenement>> FindByVilleIdEtFourchettePrix(int villeId, decimal prixMinimale, decimal prixMaximale)
    {
        string sql = @"
            SELECT * FROM evenements e
            INNER JOIN villes v on e.ville_id = v.id
            WHERE ville_id = @VilleId AND
                  prix >= @PrixMinimale AND 
                  prix < @PrixMaximale
        ";

        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            IEnumerable<Evenement> evenements = await connection.QueryAsync<Evenement, Ville, Evenement>(sql, 
                (evenement, ville) =>
                {                             
                    evenement.Ville = ville;  
                    return evenement;         
                },
                new
                {
                    VilleId = villeId,
                    PrixMinimale = prixMinimale,
                    PrixMaximale = prixMaximale
                });
            
            return evenements.ToList();
        }
    }

    public async Task<List<Evenement>> FindByFourchettePrix(decimal prixMinimale, decimal prixMaximale)
    {
        string sql = @"
            SELECT * FROM evenements e
            INNER JOIN villes v on e.ville_id = v.id
            WHERE prix >= @PrixMinimale AND 
                  prix < @PrixMaximale
        ";

        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            IEnumerable<Evenement> evenements = await connection.QueryAsync<Evenement, Ville, Evenement>(sql, 
                (evenement, ville) =>
                {                             
                    evenement.Ville = ville;  
                    return evenement;         
                },
                new
                {
                    PrixMinimale = prixMinimale,
                    PrixMaximale = prixMaximale
                });
            
            return evenements.ToList();
        }
    }

    public async Task<Evenement?> FindById(int id)
    {
        string sql = @"
            SELECT * from evenements e
            INNER JOIN villes v on e.ville_id = v.id
            WHERE e.id = @Id;
        ";

        using (IDbConnection connection = _dbContext.CreateConnection())
        {
            IEnumerable<Evenement> resultat = await connection.QueryAsync<Evenement, Ville, Evenement>(sql, 
                (evenement, ville) =>
                {                             
                    evenement.Ville = ville;  
                    return evenement;         
                }, 
                new
                {
                    Id = id
                });

            return resultat.FirstOrDefault();
        }
    }
}