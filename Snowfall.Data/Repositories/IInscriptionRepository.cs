using Snowfall.Domain.Models;

namespace Snowfall.Data.Repositories;

public interface IInscriptionRepository
{
    Task<Inscription> Create(Inscription inscription);
    Task<Inscription?> FindById(string id);
}