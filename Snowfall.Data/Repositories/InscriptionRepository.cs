using Snowfall.Data.Context;
using Snowfall.Domain.Models;

namespace Snowfall.Data.Repositories;

public class InscriptionRepository: IInscriptionRepository
{
    private DapperContext _dbContext;

    public InscriptionRepository(DapperContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<Inscription> Create(Inscription inscription)
    {
        throw new NotImplementedException();
    }

    public async  Task<Inscription?> FindById(string id)
    {
        throw new NotImplementedException();
    }
}