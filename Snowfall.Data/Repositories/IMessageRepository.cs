using Snowfall.Domain.Models;

namespace Snowfall.Data.Repositories;

public interface IMessageRepository
{
    Task<List<Message>> FindByEvenementId(int evenementId);
    Task<Message?> FindById(int id);
    Task<Message> Create(Message message);
    Task<bool> Update(Message message);
    Task<bool> Delete(int id);
}