using System.Reflection;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;

namespace Snowfall.Data.Configurations;

/// <summary>
/// Permet de configurer et d'exécuter les migrations
/// </summary>
public static class MigrationsConfig
{
    /// <summary>
    /// Extension de IServiceCollection permettant d'ajouter le service FluentMigrator à l'application
    /// </summary>
    /// <param name="services">Référence à la collection de services de l'application</param>
    /// <param name="connectionString">Informations de connexion de la BD</param>
    public static IServiceCollection AjouterMigrations(this IServiceCollection services, string connectionString)
    {
        ArgumentNullException.ThrowIfNull(services);

        services.AddLogging(c => c.AddFluentMigratorConsole())
            .AddFluentMigratorCore()
            .ConfigureRunner(c => c.AddPostgres()
                .WithGlobalConnectionString(connectionString)
                .ScanIn(Assembly.GetExecutingAssembly()).For.All());
        
        // Le service provider permettra d'obtenir le service injecté MigrationRunner. 
        IServiceProvider serviceProvider = services.BuildServiceProvider();
        
        UpdateDatabase(serviceProvider);

        return services;
    }

    /// <summary>
    /// Exécute les migrations
    /// </summary>
    /// <param name="serviceProvider">ServiceProvider permettant de récupérer les services via l'injection de dépendances</param>
    private static void UpdateDatabase(IServiceProvider serviceProvider)
    {
        /*
        On crée le "Runner" de Fluent Migrator pour exécuter les migrations.
        Un nouveau scope est créé pour s'assurer que les ressources utilisées par les
        migrations soient disposées dès que le processus est terminé.
        */
        using IServiceScope serviceScope = serviceProvider.CreateScope();
        IMigrationRunner migrationRunner = serviceScope.ServiceProvider.GetRequiredService<IMigrationRunner>();

        // Exécution des migrations
        migrationRunner.MigrateUp();
    }
}