using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202402041343)]
public class AjouterColonneVilleEvenements : Migration
{
    public override void Up()
    {
        Create.Column("ville_id")
            .OnTable("evenements")
            .AsInt64();
        
        Create.ForeignKey() 
            .FromTable("evenements").ForeignColumn("ville_id") 
            .ToTable("villes").PrimaryColumn("id");
    }

    public override void Down()
    {
        Delete.ForeignKey()
            .FromTable("evenements").ForeignColumn("ville_id")
            .ToTable("villes").PrimaryColumn("id");
    
        Delete.Column("ville_id").FromTable("evenements");
    }
}