using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202402051449)]
public class CreerMessages : Migration
{
    public override void Up()
    {
        Create.Table("messages")
            .WithColumn("id").AsInt64().PrimaryKey().Identity()
            .WithColumn("evenement_id").AsInt64()
            .WithColumn("contenu").AsString()
            .WithColumn("date_creation").AsDateTime()
            .WithColumn("date_modification").AsDateTime();

        Create.ForeignKey()
            .FromTable("messages").ForeignColumn("evenement_id")
            .ToTable("evenements").PrimaryColumn("id");
    }

    public override void Down()
    {
        Delete.ForeignKey()
            .FromTable("messages").ForeignColumn("evenement_id")
            .ToTable("evenements").PrimaryColumn("id");
        
        Delete.Table("messages");
    }
}