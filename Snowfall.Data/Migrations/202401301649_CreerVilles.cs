using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202401301649)]
public class CreerVilles : Migration
{
    public override void Up()
    {
        Create.Table("villes")
            .WithColumn("id").AsInt64().PrimaryKey().Identity()
            .WithColumn("nom").AsString(100)
            .WithColumn("pays_iso").AsFixedLengthString(2);
    }

    public override void Down()
    {
        Delete.Table("villes");
    }
}