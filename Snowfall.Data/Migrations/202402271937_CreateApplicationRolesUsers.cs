using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202402271937)]
public class CreateApplicationRolesUsers : Migration
{
    public override void Up()
    {
        Create.Table("application_roles_users")
            .WithColumn("role_id").AsInt64().PrimaryKey()
            .WithColumn("user_id").AsString(255).PrimaryKey();

        Create.ForeignKey()
            .FromTable("application_roles_users").ForeignColumn("role_id")
            .ToTable("application_roles").PrimaryColumn("id");

        Create.ForeignKey()
            .FromTable("application_roles_users").ForeignColumn("user_id")
            .ToTable("application_users").PrimaryColumn("id");
    }

    public override void Down()
    {
        Delete.ForeignKey()
            .FromTable("application_roles_users").ForeignColumn("user_id")
            .ToTable("application_users").PrimaryColumn("id");
        Delete.ForeignKey()
            .FromTable("application_roles_users").ForeignColumn("role_id")
            .ToTable("application_roles").PrimaryColumn("id");
        
        Delete.Table("application_roles_users");

    }
}