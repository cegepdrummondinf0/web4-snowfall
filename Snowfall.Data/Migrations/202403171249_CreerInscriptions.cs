using System.Data;
using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202403171249)]
public class CreerInscriptions : Migration 
{
    public override void Up()
    {
        Create.Table("inscriptions")
            .WithColumn("id").AsString(255).PrimaryKey().WithDefault(SystemMethods.NewGuid)
            .WithColumn("client_id").AsInt64()
            .WithColumn("evenement_id").AsInt64().Nullable()
            // À compléter les champs intermédiaires! À noter que pour poursuivre le niveau,
            // vous n'avez pas à avoir les champs manquants de la migration.
            // Je vous suggère même de ne pas trop vous y attarder pour le moment et d'avancer.
            // Vous pourrez le faire dans Steamy pour votre table "achats" (ou équivalent).
            .WithColumn("date_creation").AsDateTime()
            .WithColumn("date_modification").AsDateTime();

        Create.ForeignKey()
            .FromTable("inscriptions").ForeignColumn("client_id")
            .ToTable("clients").PrimaryColumn("id")
            .OnDelete(Rule.SetNull);

        Create.ForeignKey()
            .FromTable("inscriptions").ForeignColumn("evenement_id")
            .ToTable("evenements").PrimaryColumn("id")
            .OnDelete(Rule.SetNull);
        
        Create.Index("index_inscriptions_client_id")
            .OnTable("inscriptions")
            .OnColumn("client_id");
        
        Create.Index("index_inscriptions_evenement_id")
            .OnTable("inscriptions")
            .OnColumn("evenement_id");
    }

    public override void Down()
    {
        Delete.ForeignKey()
            .FromTable("inscriptions").ForeignColumn("client_id")
            .ToTable("clients").PrimaryColumn("id");

        Delete.ForeignKey()
            .FromTable("inscriptions").ForeignColumn("evenement_id")
            .ToTable("evenements").PrimaryColumn("id");

        Delete.Table("inscriptions");
    }
}