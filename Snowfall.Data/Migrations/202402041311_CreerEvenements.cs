using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202402041311)]
public class CreerEvenements : Migration
{
    public override void Up()
    {
        Create.Table("evenements")
            .WithColumn("id").AsInt64().PrimaryKey().Identity()
            .WithColumn("nom").AsString(100)
            .WithColumn("description").AsString().Nullable() 
            .WithColumn("image_path").AsString(255).Nullable()
            .WithColumn("date").AsDateTime()
            .WithColumn("prix").AsDecimal(10,2)
            .WithColumn("capacite").AsInt32();
        ;
    }

    public override void Down()
    {
        Delete.Table("evenements");
    }
}