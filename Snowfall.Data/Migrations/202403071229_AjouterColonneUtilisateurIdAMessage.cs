using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202403071229)]
public class AjouterColonneUtilisateurIdAMessage : Migration
{
    public override void Up()
    {
        Create.Column("utilisateur_id")
            .OnTable("messages")
            .AsString(255);

        Create.ForeignKey()
            .FromTable("messages").ForeignColumn("utilisateur_id")
            .ToTable("application_users").PrimaryColumn("id");
        
        Create.Index("index_messages_utilisateur_id")
            .OnTable("messages")
            .OnColumn("utilisateur_id");
        
        Create.Index("index_messages_evenement_id")
            .OnTable("messages")
            .OnColumn("utilisateur_id");
    }

    public override void Down()
    {
        Delete.ForeignKey()
            .FromTable("messages").ForeignColumn("utilisateur_id")
            .ToTable("application_users").PrimaryColumn("id");

        Delete.Column("utilisateur_id")
            .FromTable("messages");

    }
}