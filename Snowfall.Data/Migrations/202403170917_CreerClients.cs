using System.Data;
using FluentMigrator;

namespace Snowfall.Data.Migrations;

[Migration(202403170917)]
public class CreerClients : Migration
{
    public override void Up()
    {
        Create.Table("clients")
            .WithColumn("id").AsInt64().PrimaryKey().Identity()
            .WithColumn("utilisateur_id").AsString(255).Unique()
            .WithColumn("adresse").AsString(255)
            .WithColumn("ville").AsString(100)
            .WithColumn("code_postal").AsString(10)
            .WithColumn("province").AsString(100)
            .WithColumn("pays").AsString(100);

        Create.ForeignKey()
            .FromTable("clients").ForeignColumn("utilisateur_id")
            .ToTable("application_users").PrimaryColumn("id")
            .OnDelete(Rule.Cascade);
    }

    public override void Down()
    {
        Delete.ForeignKey()
            .FromTable("clients").ForeignColumn("utilisateur_id")
            .ToTable("application_users").PrimaryColumn("id");

        Delete.Table("clients");
    }

}