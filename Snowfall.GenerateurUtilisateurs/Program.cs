﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Snowfall.Data.Context;
using Snowfall.Data.Repositories;
using Snowfall.Domain.Models;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);

builder.Services.AddSingleton<DapperContext>();
builder.Services.AddScoped<IRoleStore<ApplicationRole>, RoleRepository>();
builder.Services.AddScoped<IUserStore<ApplicationUser>, UserRepository>();
builder.Services.AddIdentityCore<ApplicationUser>();

using IHost host = builder.Build();
using IServiceScope serviceScope = host.Services.CreateScope();
UserManager<ApplicationUser> userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

Console.WriteLine("Création automatique d'utilisateurs");

//1. Demander un mot de passe à ajouter à tous les utilisateurs
string? motDePasse = null;
do
{
    Console.WriteLine("Quelle est le mot de passe que vous voulez mettre aux utilisateurs ?");
    motDePasse = Console.ReadLine();
} while (string.IsNullOrWhiteSpace(motDePasse));
    
//2. Créer une liste des utilisateurs à ajouter
List<ApplicationUser> utilisateurs =
[
    new ApplicationUser()
    {
        UserName = "bobtesteur@cegepdrummund.ca",
        Email = "bobtesteur@cegepdrummund.ca",
        Prenom = "Bob",
        Nom = "Testeur"
    },
    new ApplicationUser()
    {
        UserName = "soniatesteur@cegepdrummund.ca",
        Email = "soniatesteur@cegepdrummund.ca",
        Prenom = "Sonia",
        Nom = "Testeur"
    },
    new ApplicationUser()
    {
        UserName = "alphonsetesteur@cegepdrummund.ca",
        Email = "alphonsetesteur@cegepdrummund.ca",
        Prenom = "Alphonse",
        Nom = "Testeur"
    }
];

foreach (ApplicationUser utilisateur in utilisateurs)
{
    //3. Créer les utilisateurs avec UserManager<ApplicationUser>
    IdentityResult result = await userManager.CreateAsync(utilisateur, motDePasse);
            
    if (result.Succeeded) 
    { 
        string role = "Utilisateur"; 
        await userManager.AddToRoleAsync(utilisateur, role);
        
        //4. Ajouter un rôle à l'utilisateur et afficher un message pour chaque utilisateur créé
        Console.WriteLine($"Utilisateur {utilisateur.Prenom} {utilisateur.Nom} - {utilisateur.Email} ajouté avec succès");
    }
    else
    {
        //5. Afficher un message des utilisateurs en erreur
        Console.WriteLine($"Utilisateur {utilisateur.Prenom} {utilisateur.Nom} - {utilisateur.Email} erreur - {result.Errors.First()}");
    }
}
Console.WriteLine("------------------");
Console.WriteLine("Terminé!");

await host.RunAsync();