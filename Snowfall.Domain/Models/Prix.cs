namespace Snowfall.Domain.Models;

public class Prix
{
    public required decimal SousTotal { get; set; }
    public required decimal Taxe1 { get; set; }
    public required decimal Taxe2 { get; set; }
    public required decimal Total { get; set; }
}