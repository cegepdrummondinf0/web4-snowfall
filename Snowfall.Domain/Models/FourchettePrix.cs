namespace Snowfall.Domain.Models;

public class FourchettePrix
{
    public FourchettePrix(int id, decimal valeurMaximale, string nom)
    {
        ValeurMinimale = null;
        ValeurMaximale = valeurMaximale;
        Nom = nom;
        Id = id;
    }

    public FourchettePrix(int id, decimal valeurMinimale, decimal? valeurMaximale, string nom)
    {
        ValeurMinimale = valeurMinimale;
        ValeurMaximale = valeurMaximale;
        Nom = nom;
        Id = id;
    }

    public int Id { get; private set; }

    public decimal? ValeurMinimale { get; private set; }

    public decimal? ValeurMaximale { get; private set; }

    public string Nom { get; private set; }

    public string AfficherFourchette()
    {
        if (ValeurMaximale.HasValue && ValeurMinimale.HasValue)
        {
            return $"entre {ValeurMinimale} et {ValeurMaximale}";
        }
        else if (ValeurMaximale.HasValue)
        {
            return $"<= {ValeurMaximale}";
        }
        else
        {
            return $"> {ValeurMinimale}";
        }
    }
    
    public static readonly List<FourchettePrix> FourchettePrixGlobale =
    [
        new FourchettePrix(1, 25, "Pas cher!"),
        new FourchettePrix(id: 2, 25, 50, "Abordable"),
        new FourchettePrix(id: 3,50, 100, "Gâterie"),
        new FourchettePrix(id: 4,100, null, "Luxe💰")
    ];
}