namespace Snowfall.Domain.Models;

public class Inscription
{
    public string? Id { get; set; }
    public required int ClientId { get; set; }
    public int EvenementId { get; set; }
    public required string NomEvenement { get; set; }
    public decimal Taxe1 { get; set; }
    public decimal Taxe2 { get; set; }
    public decimal SousTotal { get; set; }
    public decimal MontantTotal { get; set; }
    public DateTime DateCreation { get; set; }
    public DateTime DateModification { get; set; }
}