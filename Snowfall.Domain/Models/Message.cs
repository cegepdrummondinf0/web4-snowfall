namespace Snowfall.Domain.Models;

public class Message
{
    public int Id { get; set; }
    public int EvenementId { get; set; }
    public required string UtilisateurId { get; set; }
    public ApplicationUser? Utilisateur { get; set; }
    public required string Contenu { get; set; }
    public DateTime DateModification { get; set; }
    public DateTime DateCreation { get; set; }
}