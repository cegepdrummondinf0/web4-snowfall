using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Snowfall.Application.Dtos.Utilisateurs;
using Snowfall.Domain.Models;

namespace Snowfall.Web.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ComptesController: ControllerBase
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IMapper _mapper; 
    
    public ComptesController(UserManager<ApplicationUser> userManager, 
                             IMapper mapper)
    {
        _mapper = mapper;
        _userManager = userManager;
    }
        
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var utilisateurs = await _userManager.GetUsersInRoleAsync("Utilisateur");

        return Ok(_mapper.Map<List<UtilisateurDto>>(utilisateurs));
    }
}