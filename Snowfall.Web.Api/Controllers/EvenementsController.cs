using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Snowfall.Application.Dtos.Evenements;
using Snowfall.Application.Services;
using Snowfall.Domain.Models;

namespace Snowfall.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EvenementsController : ControllerBase
    {
        private readonly IEvenementService _evenementService;
        private readonly IMapper _mapper; 
    
        public EvenementsController(IEvenementService evenementService,
                                    IMapper mapper)
        {
            _mapper = mapper;
            _evenementService = evenementService;
        }
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Evenement> evenements = await _evenementService.GetAll();
            
            return Ok(_mapper.Map<List<EvenementDto>>(evenements));
        }
        
        /// <summary>
        /// GET /api/evenements/id
        /// Récupère un événement
        /// </summary>
        /// <param name="id">L'identifiant de l'événement</param>
        /// <returns>L'événement</returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> Show(int id)
        {
            var evenement = await _evenementService.FindById(id);

            if (evenement is null) return NotFound();

            return Ok(_mapper.Map<EvenementDto>(evenement));
        }
    }
}
