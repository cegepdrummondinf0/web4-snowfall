using AutoMapper;
using Snowfall.Application.Dtos.Evenements;
using Snowfall.Application.Dtos.Utilisateurs;
using Snowfall.Application.Dtos.Villes;
using Snowfall.Domain.Models;

namespace Snowfall.Web.Api.Configurations;

public class AutoMapperConfig : Profile
{
    public AutoMapperConfig()
    {
        CreateMap<Evenement, EvenementDto>().ReverseMap();
        CreateMap<ApplicationUser, UtilisateurDto>();
        CreateMap<Ville, VilleDto>().ReverseMap(); 
    }
}