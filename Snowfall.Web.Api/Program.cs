using Snowfall.Data.Configurations;
using Snowfall.Domain.Models;
using Snowfall.Shared;
using Snowfall.Web.Api.Configurations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers(); 

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Injection de dépendances
builder.Services.EnregistrerServices(); // [!code ++]

// Ajoute les migrations
builder.Services.AjouterMigrations(builder.Configuration.GetConnectionString("AppDatabaseConnection")!); // [!code ++]

// Dapper match underscores: nom_propriete_underscore <-> NomProprieteUnderscore
Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true; // [!code ++]

builder.Services.AddAutoMapper(typeof(AutoMapperConfig));

builder.Services.AddIdentity<ApplicationUser, ApplicationRole>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseWebAssemblyDebugging();
}

app.UseHttpsRedirection();

app.UseAuthorization(); 

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();
app.UseRouting();
app.MapControllers();
app.MapFallbackToFile("index.html");

app.Run();