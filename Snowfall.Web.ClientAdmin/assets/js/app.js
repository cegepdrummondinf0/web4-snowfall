import 'vite/modulepreload-polyfill'

// Notre Scss, contenant Bootstrap
import '../scss/app.scss'

// Importation du JavaScript de Bootstrap
import * as bootstrap from 'bootstrap'