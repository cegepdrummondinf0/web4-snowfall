using System.Net.Http.Json;
using Snowfall.Application.Dtos.Utilisateurs;

namespace Snowfall.Web.ClientAdmin.Clients;

public class CompteClient
{
    private const string BaseApiUrl = "api/comptes";
    
    private readonly HttpClient _client;
    
    public CompteClient(HttpClient client)
    {
        _client = client;
    }
    
    public async Task<List<UtilisateurDto>?> ObtenirUtilisateurs()
    {
        return await _client.GetFromJsonAsync<List<UtilisateurDto>>(BaseApiUrl);
    }
}