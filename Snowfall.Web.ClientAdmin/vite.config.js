import { defineConfig } from 'vite'
import { resolve } from 'path'

export default defineConfig({
    build: {
        manifest: true,
        rollupOptions: {
            input: resolve(__dirname, 'assets/js/app.js'),
            output: {
                entryFileNames: `assets/[name].js`,
                chunkFileNames: `assets/[name].js`,
                assetFileNames: `assets/[name].[ext]`
            }
        },
        outDir: resolve(__dirname, 'wwwroot/dist'),
    },
    resolve: {
        alias: {
            '~bootstrap': resolve(__dirname, 'node_modules/bootstrap'),
        }
    },
    server : {
        port : 5175
    }
})