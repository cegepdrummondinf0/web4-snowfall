using Snowfall.Web.ClientAdmin.Clients;

namespace Snowfall.Web.ClientAdmin.Configurations;

public static class InjectionDependancesConfig
{
    public static IServiceCollection EnregistrerServices(this IServiceCollection services)
    {
        if (services == null)
            throw new ArgumentNullException(nameof(services));

        services.AddScoped<EvenementClient>();
        services.AddScoped<CompteClient>();

        return services;
    }
}