namespace Snowfall.Application.Dtos.Villes;

#nullable disable
public class VilleDto
{
    public int Id { get; set; }
    public string Nom { get; set; }
    public string PaysIso { get; set; }
}