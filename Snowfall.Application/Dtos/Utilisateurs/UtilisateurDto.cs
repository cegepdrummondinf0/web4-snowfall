namespace Snowfall.Application.Dtos.Utilisateurs;

#nullable disable
public class UtilisateurDto
{
    public string Id { get; set; }
    public string UserName { get; set; }
    public string Email { get; set; }
    public string Prenom { get; set; }
    public string Nom { get; set; }
}