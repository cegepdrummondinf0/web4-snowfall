using Snowfall.Domain.Models;

namespace Snowfall.Application.Services;

public class PrixService : IPrixService
{
    private const decimal TauxTps = 0.05m;
    private const decimal TauxTvq = 0.095m;
    
    public Prix CalculerPrix(Evenement evenement)
    {
        decimal sousTotal = evenement.Prix;
        decimal taxe1 = CalculerTaxe1(sousTotal);
        decimal taxe2 = CalculerTaxe2(sousTotal);
        decimal total = CalculerTotal(sousTotal);

        return new Prix
        {
            SousTotal = sousTotal,
            Taxe1 = Math.Round(taxe1, 2),
            Taxe2 = Math.Round(taxe2, 2),
            Total = Math.Round(total, 2)
        };
    }

    private decimal CalculerTaxe1(decimal prix)
    {
        return prix * TauxTps;
    }

    private decimal CalculerTaxe2(decimal prix)
    {
        return (prix + CalculerTaxe1(prix)) * TauxTvq;
    }

    private decimal CalculerTotal(decimal prix)
    {
        return prix + CalculerTaxe1(prix) + CalculerTaxe2(prix);
    }
}