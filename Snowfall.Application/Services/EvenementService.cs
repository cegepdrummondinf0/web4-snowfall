using Snowfall.Data.Repositories;
using Snowfall.Domain.Models;

namespace Snowfall.Application.Services;

public class EvenementService : IEvenementService
{
    private readonly IEvenementsRepository _evenementRepository; 

    public EvenementService(IEvenementsRepository evenementRepository) 
    {
        _evenementRepository = evenementRepository; 
    }
    
    public async Task<List<Evenement>> GetAll()
    {
        return await _evenementRepository.GetAll();
    }
    
    public async Task<List<Evenement>> FindByVilleId(int? villeId)
    {
        if (villeId.HasValue)
        {
            return await _evenementRepository.FindByVilleId(villeId.Value);
        }

        return await _evenementRepository.GetAll();
    }
    
    public async Task<List<Evenement>> FindByVilleIdEtFourchettePrix(int? villeId, int? fourchettePrixId)
    {
        if (villeId.HasValue && fourchettePrixId.HasValue)
        {
            var fourchettePrix = FourchettePrix.FourchettePrixGlobale.Single(prix => prix.Id == fourchettePrixId.Value);
            return await _evenementRepository.FindByVilleIdEtFourchettePrix(
                villeId.Value, 
                fourchettePrix.ValeurMinimale ?? 0, 
                fourchettePrix.ValeurMaximale ?? decimal.MaxValue);
        } 
        else if (villeId.HasValue)
        {
            return await _evenementRepository.FindByVilleId(villeId.Value);
        }
        else if (fourchettePrixId.HasValue)
        {
            var fourchettePrix = FourchettePrix.FourchettePrixGlobale.Single(prix => prix.Id == fourchettePrixId.Value);
            return await _evenementRepository.FindByFourchettePrix(
                fourchettePrix.ValeurMinimale ?? 0, 
                fourchettePrix.ValeurMaximale ?? decimal.MaxValue);
        }

        return await _evenementRepository.GetAll();
    }

    public async Task<Evenement?> FindById(int id)
    {
        return await _evenementRepository.FindById(id);
    }
}