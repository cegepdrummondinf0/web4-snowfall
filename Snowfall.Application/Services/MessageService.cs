using Snowfall.Data.Repositories;
using Snowfall.Domain.Models;

namespace Snowfall.Application.Services;

public class MessageService : IMessageService
{
    private readonly IMessageRepository _messageRepository; 
    
    public MessageService(IMessageRepository messageRepository) 
    { 
        _messageRepository = messageRepository; 
    } 
    
    public async Task<List<Message>> FindByEvenementId(int evenementId)
    {
        return await _messageRepository.FindByEvenementId(evenementId);
    }
    
    public async Task<Message?> FindById(int id)
    {
        return await _messageRepository.FindById(id);
    }
    
    public async Task<Message> Create(Message message)
    {
        return await _messageRepository.Create(message);
    }
    
    public async Task<bool> Update(Message message)
    {
        return await _messageRepository.Update(message);
    }
    
    public async Task<bool> Delete(int id)
    {
        return await _messageRepository.Delete(id);
    }
}