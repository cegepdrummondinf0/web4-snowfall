using Snowfall.Domain.Models;

namespace Snowfall.Application.Services;

public interface IPrixService
{
    Prix CalculerPrix(Evenement evenement);
}