using Snowfall.Domain.Models;

namespace Snowfall.Application.Services;

public interface IInscriptionService
{
    public Task<Inscription?> FindById(string id);
    public Task<Inscription> Create(Inscription inscription);
}