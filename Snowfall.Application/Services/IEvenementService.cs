using Snowfall.Domain.Models;

namespace Snowfall.Application.Services;

public interface IEvenementService
{
    Task<List<Evenement>> GetAll();
    Task<List<Evenement>> FindByVilleId(int? villeId);
    Task<List<Evenement>> FindByVilleIdEtFourchettePrix(int? villeId, int? fourchettePrixId);
    Task<Evenement?> FindById(int id);
}