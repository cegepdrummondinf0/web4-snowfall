using Snowfall.Data.Repositories;
using Snowfall.Domain.Models;

namespace Snowfall.Application.Services;

public class InscriptionService : IInscriptionService
{
    private readonly IInscriptionRepository _inscriptionRepository;
    
    public InscriptionService(IInscriptionRepository inscriptionRepository)
    {
        _inscriptionRepository = inscriptionRepository;
    }
    
    public async Task<Inscription?> FindById(string id)
    {
        return await _inscriptionRepository.FindById(id);
    }

    public async Task<Inscription> Create(Inscription inscription)
    {
        return await _inscriptionRepository.Create(inscription);
    }
}