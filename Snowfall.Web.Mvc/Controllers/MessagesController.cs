using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Snowfall.Application.Services;
using Snowfall.Domain.Models;
using Snowfall.Web.Mvc.Extensions;
using Snowfall.Web.Mvc.Models.Messages;

namespace Snowfall.Web.Mvc.Controllers;

[Route("evenements/{evenementId:int}/[controller]")] 
public class MessagesController : Controller
{
    private readonly IMessageService _messageService; 
    private readonly IEvenementService _evenementService;

    public MessagesController(
        IMessageService messageService,
        IEvenementService evenementService) 
    { 
        _messageService = messageService;
        _evenementService = evenementService;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index(int evenementId) 
    {
        List<Message> messages = await _messageService.FindByEvenementId(evenementId);
        return PartialView("_Messages", messages);
    }
    
    [HttpGet("[action]")]
    [Authorize]
    public async Task<IActionResult> New(int evenementId)
    {
        var evenement = await _evenementService.FindById(evenementId); 
        if (evenement == null) return NotFound(); 
        
        var viewModel = new CreerMessageViewModel()
        {
            EvenementId = evenementId
        };
        return View(viewModel);
    }
    
    [HttpPost]
    [Authorize]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(int evenementId, CreerMessageViewModel viewModel)
    {
        var evenement = await _evenementService.FindById(evenementId);
        if (evenement == null) return NotFound();
        
        if (!ModelState.IsValid) 
        { 
            return View(nameof(New), viewModel); 
        } 
        
        Message message = new ()
        {
            UtilisateurId = User.Identity!.Id(),
            EvenementId = viewModel.EvenementId,
            Contenu = viewModel.Contenu
        };

        await _messageService.Create(message);

        return RedirectToAction("Show", "Evenements", new { Id = evenementId });
    }
    
    [HttpGet("{id:int}/edit")]
    [Authorize]
    public async Task<IActionResult> Edit(int id, int evenementId)
    {
        var message = await ObtenirMessage(id, evenementId);
        if (message == null) return NotFound();
    
        var viewModel = new ModifierMessageViewModel
        {
            Id = message.Id,
            EvenementId = message.EvenementId,
            Contenu = message.Contenu
        };

        return View(viewModel);
    }
    
    [HttpPatch("{id:int}")]
    [Authorize]
    public async Task<IActionResult> Update(int id, int evenementId, ModifierMessageViewModel viewModel)
    {
        var message = await ObtenirMessage(id, evenementId);
        if (message == null) return NotFound();   
        
        if (!ModelState.IsValid)
        {
            return View(nameof(Edit), viewModel);
        }

        message.Contenu = viewModel.Contenu;
        await _messageService.Update(message);
    
        return RedirectToAction("Show", "Evenements", new { id = evenementId});
    }
    
    [HttpGet("{id:int}/[action]")]
    [Authorize]
    public async Task<IActionResult> ConfirmationDelete(int evenementId, int Id)
    {
        var message = await _messageService.FindById(Id);

        if (message == null) return NotFound();
        
        return PartialView("_ModalConfirmationDelete", message);
    }
    
    [HttpDelete("{id:int}")]
    [Authorize]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Delete(int id, int evenementId)
    {
        var message = await ObtenirMessage(id, evenementId);
        if (message == null) return NotFound();

        await _messageService.Delete(message.Id);
    
        return RedirectToAction("Show", "Evenements", new { id = evenementId});
    }
    
    private async Task<Message?> ObtenirMessage(int id, int evenementId)
    {
        var evenement = await _evenementService.FindById(evenementId);
        var message = await _messageService.FindById(id);

        if (evenement == null || message == null) return null;
        if (message.UtilisateurId != User.Identity?.Id()) return null;

        return message;
    }
}