using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Snowfall.Domain.Models;
using Snowfall.Web.Mvc.Models.Comptes;

namespace Snowfall.Web.Mvc.Controllers;
[Route("[controller]")]
public class ComptesController : Controller
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;

    public ComptesController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
    {
        _userManager = userManager;
        _signInManager = signInManager;
    }

    [HttpGet("[action]")]
    public IActionResult New()
    {
        return View();
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(CreerCompteViewModel viewModel)
    {
        if (ModelState.IsValid)
        {
            var user = new ApplicationUser()
            {
                UserName = viewModel.Email,
                Email = viewModel.Email,
                Prenom = viewModel.Prenom,
                Nom = viewModel.Nom,
                Client = new Client
                {
                    Adresse = viewModel.Adresse,
                    Ville = viewModel.Ville,
                    CodePostal = viewModel.CodePostal,
                    Province = viewModel.Province,
                    Pays = viewModel.Pays
                }
            };
            
            IdentityResult result = await _userManager.CreateAsync(user, viewModel.Password);
            
            if (result.Succeeded) 
            { 
                string role = "Utilisateur";
                await _userManager.AddToRoleAsync(user, role);
                await _signInManager.SignInAsync(user, isPersistent: true); 
                return RedirectToAction("Index", "Evenements"); 
            } 
        }
    
        return View(nameof(New), viewModel);
    }
}