using Microsoft.AspNetCore.Mvc;
using Snowfall.Application.Services;
using Snowfall.Domain.Models;
using Snowfall.Web.Mvc.Models.Evenements;

namespace Snowfall.Web.Mvc.Controllers;

[Route("[controller]")]
public class EvenementsController : Controller
{
    private IEvenementService _evenementService;
    private readonly IVilleService _villeService;
    
    public EvenementsController(
        IEvenementService evenementService,
        IVilleService villeService)
    {
        _evenementService = evenementService;
        _villeService = villeService;
    }
    
    [Route("/")]
    public async Task<IActionResult> Index(int? ville, int? fourchette)
    {
        List<Evenement> evenements = await _evenementService.FindByVilleIdEtFourchettePrix(ville, fourchette);
        List<Ville> villes = await _villeService.GetAll();
        
        var viewModel = new EvenementsIndexViewModel
        {
            Villes = villes,
            Evenements = evenements,
            FiltreVilleId = ville,
            FourchettePrix = FourchettePrix.FourchettePrixGlobale,
            FiltreFourchettePrixId = fourchette
        };
    
        return View(viewModel);
    }
    
    [HttpGet("{id:int}")]
    public async Task<IActionResult> Show(int id, string? option)
    {
        Evenement? evenement = await _evenementService.FindById(id);
        if (evenement == null)
        {
            return NotFound(); 
        }

        return View(evenement);
    }
    
    [HttpGet("{id:int}/edit")]
    public IActionResult Edit(int id)
    {
        return Content("Vue Edit de EvenementsController, id: " + id);
    }
}