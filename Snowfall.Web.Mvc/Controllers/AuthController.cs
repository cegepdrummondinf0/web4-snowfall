using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Snowfall.Domain.Models;
using Snowfall.Web.Mvc.Models.Auth;

namespace Snowfall.Web.Mvc.Controllers;

[Route("[controller]")]
public class AuthController : Controller
{
    private readonly SignInManager<ApplicationUser> _signInManager;
    
    public AuthController(SignInManager<ApplicationUser> signInManager)
    {
        _signInManager = signInManager;
    }
    
    // GET /auth/connexion
    [HttpGet("[action]")]
    public IActionResult Connexion(string? returnUrl)
    {
        if (!string.IsNullOrWhiteSpace(returnUrl))
        {
          ViewBag.ReturnUrl = returnUrl;
        }
        return View();
    }
    
    // GET /auth/deconnexion
    [HttpPost("[action]")]
    [Authorize]
    public async Task<IActionResult> Deconnexion()
    {
        await _signInManager.SignOutAsync();
        HttpContext.Session.Clear(); 
        
        return RedirectToAction("Index", "Evenements");
    }
    
    // POST /auth
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Authentifier(ConnexionViewModel viewModel, string? returnUrl)
    {
        if (ModelState.IsValid)
        {
            var result = await _signInManager.PasswordSignInAsync(
                viewModel.Email,
                viewModel.Password,
                true,
                lockoutOnFailure: false
            );

            if (result.Succeeded)
            {
                if (!string.IsNullOrWhiteSpace(returnUrl))
                {
                    return Redirect(returnUrl);
                }

                return RedirectToAction("Index", "Evenements");
            }

            ModelState.AddModelError("Authentification.Echec", Resources.Controllers.AuthController.Authentification_Echec);
            return View(nameof(Connexion), viewModel);
        }
        
        return View(nameof(Connexion), viewModel);
    }
}