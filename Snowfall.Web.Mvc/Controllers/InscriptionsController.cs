using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Snowfall.Application.Services;
using Snowfall.Domain.Models;
using Snowfall.Web.Mvc.Models.Inscriptions;

namespace Snowfall.Web.Mvc.Controllers;

[Route("[controller]")]
public class InscriptionsController : Controller
{
    private readonly IEvenementService _evenementService;
    private readonly IInscriptionService _inscriptionService;
    private readonly IPrixService _prixService;
    private readonly UserManager<ApplicationUser> _userManager;

    public InscriptionsController(
        IEvenementService evenementService,
        IInscriptionService inscriptionService,
        IPrixService prixService,
        UserManager<ApplicationUser> userManager)
    {
        _evenementService = evenementService;
        _inscriptionService = inscriptionService;
        _userManager = userManager;
        _prixService = prixService;
    }
    
    [HttpPost("[action]")]
    [Authorize]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Acheter(int id)
    {
        var evenement = await _evenementService.FindById(id);

        if (evenement == null) return BadRequest();

        HttpContext.Session.SetInt32("Panier", id); 
        
        return RedirectToAction(nameof(Checkout));
    }
    
    [HttpPost("[action]")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> AcheterPlusTard(int id)
    {
        var evenement = await _evenementService.FindById(id);

        if (evenement == null) return BadRequest();

        HttpContext.Session.SetInt32("Panier", id); 
        
        return RedirectToAction("Connexion", "Auth", new { returnUrl = "Inscriptions/Checkout" });
    }
    
    [HttpGet("[action]")]
    [Authorize]
    public async Task<IActionResult> Checkout()
    {
        var evenementId = HttpContext.Session.GetInt32("Panier");

        if (evenementId.HasValue && User.Identity?.Name != null)
        {
            var evenement = await _evenementService.FindById(evenementId.Value);
            var utilisateur = await _userManager.FindByNameAsync(User.Identity.Name);
        
            if (evenement == null || utilisateur == null)
                return BadRequest();

            Prix prix = _prixService.CalculerPrix(evenement); 
            
            var viewModel = new InscriptionViewModel()
            {
                Utilisateur = utilisateur,
                Evenement = evenement,
                Prix = prix
            };
        
            return View(viewModel);
        }

        return RedirectToAction("Index", "Evenements");
    }
    
    [HttpDelete]
    [Authorize]
    [ValidateAntiForgeryToken]
    public IActionResult Delete()
    {
        HttpContext.Session.Remove("Panier"); 

        return RedirectToAction("Index", "Evenements");
    }
}