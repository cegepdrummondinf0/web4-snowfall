using System.ComponentModel.DataAnnotations;

namespace Snowfall.Web.Mvc.Models.Comptes;

#nullable disable
public class CreerCompteViewModel
{
    [Required(ErrorMessage = "Prenom.Required")] 
    [Display(Name = "Prénom", Prompt = "Prénom")]
    public string Prenom { get; set; }
    
    [Required(ErrorMessage = "Nom.Required")] 
    [Display(Name = "Nom", Prompt = "Nom")]
    public string Nom { get; set; }
    
    [Required(ErrorMessage = "Adresse.Required")]
    [Display(Name = "Adresse", Prompt = "Adresse")]
    public required string Adresse { get; set; }

    [Required(ErrorMessage = "Ville.Required")]
    [Display(Name = "Ville", Prompt = "Ville")]
    public required string Ville { get; set; }

    [Required(ErrorMessage = "CodePostal.Required")]
    [Display(Name = "Code postal", Prompt = "Code postal")]
    public required string CodePostal { get; set; }

    [Required(ErrorMessage = "Province.Required")]
    [Display(Name = "Province", Prompt = "Province")]
    public required string Province { get; set; }

    [Required(ErrorMessage = "Pays.Required")]
    [Display(Name = "Pays", Prompt = "Pays")]
    public required string Pays { get; set; }

    [Required(ErrorMessage = "Email.Required")] 
    [EmailAddress]
    [Display(Name = "Courriel", Prompt = "Courriel")]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Password.Required")] 
    [StringLength(16, MinimumLength = 8, ErrorMessage = "Password.Length")]
    [DataType(DataType.Password)]
    [Display(Name = "Mot de passe", Prompt = "Mot de passe")]
    public string Password { get; set; }
    
    [Required(ErrorMessage = "ConfirmPassword.Required")] 
    [DataType(DataType.Password)]
    [Compare("Password", ErrorMessage = "Password.Compare")] 
    [Display(Name = "Confirmation du mot de passe", Prompt = "Confirmation du mot de passe")]
    public string ConfirmPassword { get; set; }
}