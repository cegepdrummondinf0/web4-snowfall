using Snowfall.Domain.Models;

namespace Snowfall.Web.Mvc.Models.Inscriptions;

#nullable disable
public class InscriptionViewModel
{
    public ApplicationUser Utilisateur { get; set; }
    public Evenement Evenement { get; set; }
    public Prix Prix { get; set; } 
}