using Snowfall.Domain.Models;

namespace Snowfall.Web.Mvc.Models.Evenements;

public class EvenementsIndexViewModel
{
    public required List<Evenement> Evenements { get; set; }

    public required List<Ville> Villes { get; set; }

    public int? FiltreVilleId { get; set; }

    public required List<FourchettePrix> FourchettePrix { get; set; }

    public required int? FiltreFourchettePrixId { get; set; }
}