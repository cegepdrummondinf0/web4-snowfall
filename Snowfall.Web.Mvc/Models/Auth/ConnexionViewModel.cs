using System.ComponentModel.DataAnnotations;

namespace Snowfall.Web.Mvc.Models.Auth;

#nullable disable
public class ConnexionViewModel
{
    [Required(ErrorMessage = "Email.Required")]
    [EmailAddress]
    [Display(Name = "Courriel", Prompt = "Courriel")]
    public string Email { get; set; }

    [Required(ErrorMessage = "Password.Required")]
    [DataType(DataType.Password)]
    [Display(Name = "Mot de passe", Prompt = "Mot de passe")]
    public string Password { get; set; }
}