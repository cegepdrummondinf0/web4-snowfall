using System.ComponentModel.DataAnnotations;

namespace Snowfall.Web.Mvc.Models.Messages;

public class ModifierMessageViewModel : CreerMessageViewModel
{
    [Required]
    public int Id { get; set; }
}