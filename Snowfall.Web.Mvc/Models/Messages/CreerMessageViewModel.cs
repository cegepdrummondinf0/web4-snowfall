using System.ComponentModel.DataAnnotations;
using Snowfall.Web.Mvc.Resources.Models.Messages;

namespace Snowfall.Web.Mvc.Models.Messages;

#nullable disable
public class CreerMessageViewModel
{
    [Required]
    public int EvenementId { get; set; }
    
    [Required(ErrorMessageResourceType = typeof(MessageViewModelResources), ErrorMessageResourceName = "Contenu_Required")] 
    [Display(ResourceType = typeof(MessageViewModelResources), Name = "Contenu", Prompt = "Contenu")] 
    public string Contenu { get; set; }
}