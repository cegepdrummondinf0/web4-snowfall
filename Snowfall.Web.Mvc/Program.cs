using Microsoft.AspNetCore.Identity;
using Snowfall.Application.Services;
using Snowfall.Data.Claims;
using Snowfall.Data.Configurations;
using Snowfall.Data.Context;
using Snowfall.Data.Repositories;
using Snowfall.Domain.Models;
using Snowfall.Shared;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration; 

// Injection de dépendances
builder.Services.EnregistrerServices();

builder.Services
    .AddIdentity<ApplicationUser, ApplicationRole>()
    .AddClaimsPrincipalFactory<ApplicationClaimsPrincipalFactory>();

builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = "/Auth/Connexion";
});

builder.Services.AddSession();

builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");
builder.Services
    .AddControllersWithViews()
    .AddMvcLocalization() 
    .AddDataAnnotationsLocalization(); 
// Ajoute les migrations
builder.Services.AjouterMigrations(configuration.GetConnectionString("AppDatabaseConnection")!); 

// Dapper match underscores: nom_propriete_underscore <-> NomProprieteUnderscore
Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSession();

// permets les méthodes PUT/PATCH/DELETE dans les formulaires. À mettre avant UseRouting()!
app.UseHttpMethodOverride(new() { FormFieldName = "_method" }); 
app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Evenements}/{action=Index}/{id?}");

app.Run();
