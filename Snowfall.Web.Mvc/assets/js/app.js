import 'vite/modulepreload-polyfill'

import '../scss/app.scss'

// Importation du JavaScript de Bootstrap
import * as bootstrap from 'bootstrap'
window.bootstrap = bootstrap;

import htmx from 'htmx.org'