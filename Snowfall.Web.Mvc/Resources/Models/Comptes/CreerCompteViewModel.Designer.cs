﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Snowfall.Web.Mvc.Resources.Models.Comptes {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CreerCompteViewModel {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CreerCompteViewModel() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Snowfall.Resources.Models.Comptes.CreerCompteViewModel", typeof(CreerCompteViewModel).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Le champ confirmation est requis.
        /// </summary>
        internal static string ConfirmPassword_Required {
            get {
                return ResourceManager.GetString("ConfirmPassword.Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Le champ courriel est requis.
        /// </summary>
        internal static string Email_Required {
            get {
                return ResourceManager.GetString("Email.Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Le champ nom est requis.
        /// </summary>
        internal static string Nom_Required {
            get {
                return ResourceManager.GetString("Nom.Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Les mots de passe doivent être identiques.
        /// </summary>
        internal static string Password_Compare {
            get {
                return ResourceManager.GetString("Password.Compare", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Le champ mot de passe est requis.
        /// </summary>
        internal static string Password_Required {
            get {
                return ResourceManager.GetString("Password.Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Le champ prénom est requis.
        /// </summary>
        internal static string Prenom_Required {
            get {
                return ResourceManager.GetString("Prenom.Required", resourceCulture);
            }
        }
    }
}
